<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPagosUnicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('unique_payments', function (Blueprint $table) {
           $table->increments('id');
           $table->dateTime('date');
           $table->integer('service_id');
           $table->integer('monto_pago');
           $table->integer('rut');
           $table->string('digito_verificador');
           $table->string('nombre');
           $table->string('telefono')->nullable();
           $table->string('email')->nullable();
           $table->string('forma_pago')->nullable();
           $table->timestamps();
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
