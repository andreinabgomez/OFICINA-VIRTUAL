<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('clients', function (Blueprint $table) {
           $table->increments('id');
           $table->string('id_oficina');
           $table->integer('rut');
           $table->string('digito_verificador');
           $table->string('nombre');
           $table->date('fecha_nacimiento');
           $table->string('nacionalidad');
           $table->string('telefono_fijo')->nullable();
           $table->string('telefono_movil');
           $table->string('email');
           $table->string('email_alternativo')->nullable();
           $table->string('domicilio');
           $table->string('comuna');
           $table->string('empresa')->nullable();
           $table->string('rut_empresa')->nullable();
           $table->string('actividad_empresa')->nullable();
           $table->boolean('dicom')->default(false);
           $table->integer('user_id');
           $table->timestamps();
       });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('clients');;
     }
}
