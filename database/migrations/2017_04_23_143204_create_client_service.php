<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('client_services', function (Blueprint $table) {
          $table->increments('id');
          $table->dateTime('date');
          $table->dateTime('fecha_vencimiento');
          $table->dateTime('fecha_proxima_cuota');
          $table->integer('client_id');
          $table->integer('dia_pago');
          $table->integer('service_id');
          $table->string('frecuencia');
          $table->integer('monto_pago');
          $table->integer('inscripcion');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('client_services');;
     }
}
