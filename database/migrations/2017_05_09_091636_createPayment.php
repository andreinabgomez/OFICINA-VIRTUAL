<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('payments', function (Blueprint $table) {
             $table->increments('id');
             $table->dateTime('fecha_pago');
             $table->integer('interes')->default(0);
             $table->integer('gasto_cobranza')->default(0);
             $table->integer('descuento')->default(0);
             $table->integer('total');
             $table->string('forma_pago');
             $table->integer('client_service_id');
             $table->integer('monto_cuota')->default(0);
             $table->dateTime('fecha_vencimiento');
             $table->timestamps();
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
         Schema::dropIfExists('payments');
     }
}
