<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fecha_vencimiento')->nullable();
            $table->dateTime('fecha_pago')->nullable();
            $table->boolean('payment')->default(false);
            $table->integer('interes')->default(0);
            $table->integer('gasto_cobranza')->default(0);
            $table->integer('descuento')->default(0);
            $table->integer('total')->nullable();
            $table->integer('client_service_id');
            $table->integer('payment_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
}
