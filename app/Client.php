<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  protected $fillable = [
    'id_oficina', 'user_id','digito_verificador',
    'rut','nombre','fecha_nacimiento','nacionalidad',
    'telefono_fijo','telefono_movil','email','email_alternativo','domicilio',
    'comuna','empresa','rut_empresa','actividad_empresa','dicom'
  ];

  public function calls()
  {
    return $this->hasMany('App\ClientCall')->orderBy('date','desc');
  }

  public function clientservices()
  {
    return $this->hasMany('App\ClientService')->orderBy('fecha_vencimiento');
  }

  public function clientservicesvencidos()
  {
    return $this->clientservices->where('fecha_vencimiento', '<', Carbon::now())->count();
  }

  public function dias_vencido()
  {
    $services = $this->clientservices->where('fecha_vencimiento', '<', Carbon::now());
    $dias = 0;
    foreach ($services as $service) {
      if($dias < $service->dias_vencidos()){
        $dias = $service->dias_vencidos();
      }
    }
    return $dias;
  }

  public function usuario()
  {
      return $this->belongsTo('App\User');
  }
}
