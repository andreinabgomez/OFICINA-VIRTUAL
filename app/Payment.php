<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use App\ClientService;

class Payment extends Model
{
  protected $fillable = [
      'fecha_pago', 'interes','gasto_cobranza','descuento','total','client_service_id', 'forma_pago', 'fecha_vencimiento', 'monto_cuota'
  ];

  public function fees()
  {
    return $this->hasMany('App\Fee')->orderBy('fecha_vencimiento','desc');
  }


  public function client_service()
  {
      return $this->belongsTo('App\ClientService');
  }



}
