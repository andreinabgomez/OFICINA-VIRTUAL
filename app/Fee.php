<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Fee extends Model
{
  protected $fillable = [
      'fecha_vencimiento', 'fecha_pago', 'payment','interes','gasto_cobranza','descuento','total','client_service_id', 'payment_id', 'monto_cuota'
  ];

  public function client_service()
  {
      return $this->belongsTo('App\ClientService');
  }

  public function dias_vencidos()
  {
    $date = Carbon::now();
    $date_vencida = Carbon::parse($this->fecha_vencimiento);
    if($this->fecha_vencimiento < $date){
      return $date_vencida->diffInDays($date);
    }
    else {
      return 0;
    }
  }

  public function intereses()
  {
    $date = Carbon::now();
    if($this->fecha_vencimiento < $date && $this->payment == false) {
      $dias_vencidos = $this->dias_vencidos();
      $information = Information::all()->last();
      $interes = (($information->interes/100) * $this->client_service->monto_pago)*$dias_vencidos;
      return floor($interes);
    }
    else {
      return 0;
    }
  }

  public function gastos()
  {
    $date = Carbon::now();
    $date_vencida = Carbon::parse($this->fecha_vencimiento);
    if($this->fecha_vencimiento < $date && $this->payment == false) {
      $month_vencidos = $date_vencida->diffInMonths($date);
      if($month_vencidos > 0 && $this->payment == false)
      {
        $meses_vencidos = floor($this->dias_vencidos()/30);
        $information = Information::all()->last();
        $gasto = (($information->gasto_cobranza/100) * $this->client_service->monto_pago);
        return floor($gasto);
      }
      else {
        return 0;
      }
    }
    else {
      return 0;
    }
  }

  public function totales()
  {
    $monto = $this->intereses()+$this->client_service->monto_pago+$this->gastos();
    return $monto;
  }

}
