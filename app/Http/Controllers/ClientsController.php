<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Client;
Use App\ClientService;
Use App\Fee;
Use App\User;
Use App\Payment;
use DateTime;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class ClientsController extends Controller
{
  public function index()
  {
    $data = request()->all();
    if ($data != null && isset($data['search']))
    {
      $clients = ClientService::join('clients', 'client_services.client_id', '=', 'clients.id')->where('clients.nombre','like','%'.$data['search'].'%')->orWhere('clients.rut', 'like','%'.$data['search'].'%')->orWhere('clients.email', 'like','%'.$data['search'].'%')->orWhere('clients.rut_empresa', 'like','%'.$data['search'].'%')->select('client_services.*')->paginate(500);
    }
    elseif (isset($data['id'])) {
      $clients = ClientService::join('clients', 'client_services.client_id', '=', 'clients.id')->where('clients.id_oficina','=', $data['id'])->paginate(50);
    }
    else{
      $clients = ClientService::paginate(10);
    }
    return view('clientes_index',compact('clients'));
  }

  public function indexEliminar()
  {
    $data = request()->all();
    if ($data != null && isset($data['search']))
    {
      $clients = Client::where('nombre','like','%'.$data['search'].'%')->orWhere('id_oficina', '=', $data['search'])->paginate(50);
    }
    else {
      $clients = Client::paginate(50);
    }
      return view('clientes_eliminar',compact('clients'));
  }

  public function indexBitacora()
  {
    $data = request()->all();
    if ($data != null && isset($data['search']))
    {
      $clients = Client::where('nombre','like','%'.$data['search'].'%')->orWhere('id_oficina', '=', $data['search'])->paginate(50);
    }
    else {
      $clients = Client::paginate(50);
    }
    return view('clientes_bitacora',compact('clients'));
  }

  public function indexPago()
  {
    $clients = Payment::orderBy('fecha_pago','desc')->paginate(10);
    return view('clientes_pago',compact('clients'));
  }

  public function indexContrato()
  {
    $today = Carbon::now();
    $clients = ClientService::whereMonth('date', '=', date('m', strtotime($today)))->groupBy('client_id')->select('client_id', DB::raw('count(*) as total'))->get();
    return view('nuevos_contratos',compact('clients'));
  }


  public function indexUsuarios()
  {
    $clients = User::where('type', '=', 'Empleado')->orWhere('type', '=', 'Administrador')->orWhere('type', '=', 'Cobranza')->get();
    return view('usuarios_index',compact('clients'));
  }

  public function indexProximos()
  {
    $date = Carbon::now()->addWeeks(3);
    $today = Carbon::now();
    $clients = ClientService::where('fecha_vencimiento', '<', $date)->where('fecha_proxima_cuota', '>', $today)->join('clients',  'client_services.client_id', '=', 'clients.id')->orderBy('clients.id_oficina')->get(['client_services.*']);
    return view('clientes_proximos_vencidos',compact('clients'));
  }

  public function indexVencidos()
  {
    $date = Carbon::now();
    $data = request()->all();
    if ($data != null)
    {
      $clients = ClientService::join('clients', 'client_services.client_id', '=', 'clients.id')->where('fecha_vencimiento', '<', $date)->whereNotNull('fecha_vencimiento')->where('clients.nombre','like','%'.$data['search'].'%')->get();
      if($clients->isEmpty())
      {
        $clients = ClientService::join('clients', 'client_services.client_id', '=', 'clients.id')->where('fecha_vencimiento', '<', $date)->whereNotNull('fecha_vencimiento')->where('clients.id_oficina', '=', $data['search'])->get();
      }
    }
    else {
      $clients = ClientService::where('client_services.status','=','1')->where('fecha_vencimiento', '<', $date)->whereNotNull('fecha_vencimiento')->orderBy('client_id')->get();
    }
    return view('clientes_vencidos',compact('clients'));
  }

  public function showClient($id)
  {
     return view('clientes_show', ['client' => Client::findOrFail($id)]);
  }

  public function editClient($id)
  {
     return view('clientes_edit', ['client' => Client::findOrFail($id)]);
  }

  public function updateClient($id)
  {
    $client = Client::find($id);
    $data = request()->all();
    $client->id_oficina = $data['id'];
    $client->rut = $data['rut'];
    $client->digito_verificador = $data['digito'];
    $client->nombre = $data['name'];
    $client->fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $data['date']);
    $client->nacionalidad = $data['nacionalidad'];
    $client->telefono_fijo = $data['fijo'];
    $client->telefono_movil = $data['movil'];
    $client->email = $data['email'];
    $client->email_alternativo = $data['alternativo'];
    $client->domicilio = $data['domicilio'];
    $client->comuna = $data['comuna'];
    $client->empresa = $data['empresa'];
    $client->rut_empresa = $data['rut-empresa'];
    $client->actividad_empresa = $data['actividad_empresa'];
    $client->save();
    return Redirect::route('cliente', array('id' => $id));
  }

  public function showCalls($id)
  {
     return view('bitacoras_index', ['client' => Client::findOrFail($id)]);
  }

  public function showPayments($id)
  {
     return view('pagos_index', ['client' => Client::findOrFail($id)]);
  }

  public function deleteClient($id)
  {
    $client = Client::find($id);
    $id_user = $client ->user_id;
    $user = User::find($id_user);
    foreach ($client ->clientservices as $service) {
      $deletedFees = Fee::where('client_service_id', $service->id)->delete();
      $deletedPayments = Payment::where('client_service_id', $service->id)->delete();
    }
    $deletedServices = ClientService::where('client_id', $id)->delete();
    $client->delete();
    $user -> delete();
    return Redirect::to("/clientes_eliminar");
  }

  public function deleteUser($id)
  {
    $user = User::find($id);
    $user -> delete();
    return Redirect::to("/usuarios");
  }

  public function registrarDicom($id)
  {
    $client = Client::find($id);
    $client->dicom = true;
    $client->save();
    return Redirect::route('cliente', array('id' => $client->id));
  }

  public function eliminarDicom($id)
  {
    $client = Client::find($id);
    $client->dicom = false;
    $client->save();
    return Redirect::route('cliente', array('id' => $client->id));
  }

  public function createClient()
  {
     $data = request()->all();
     $frecuencia = 0;
     if($data['frecuencia'] == 'Semestral')
      {
          $frecuencia = 6;
      }
      elseif($data['frecuencia'] =='Anual')
      {
          $frecuencia = 12;
      }
      elseif($data['frecuencia'] =='Trimestral')
      {
          $frecuencia = 3;
      }
      elseif($data['frecuencia'] =='Mensual')
      {
          $frecuencia = 1;
      }
     $fecha_pago = Carbon::createFromFormat('d/m/Y', $data['creacion_servicio']);
     $day = $data['dia'];
     $month = $fecha_pago->month+$frecuencia;
     $year = $fecha_pago->year;
     if($data['frecuencia'] != 'Unica')
      {
          $fecha_vencimiento = date('Y-m-d', mktime(0,0,0, $month, $day, $year));
          $fecha_proxima = Carbon::parse($fecha_vencimiento)->addMonths($frecuencia);
      }
      else {
        $fecha_vencimiento = null;
        $fecha_proxima = null;
      }
     $user = User::create([
         'name' => $data['name'],
         'email' => $data['email'],
         'password' => bcrypt($data['rut']),
         'type' => "Cliente",
     ]);
     $client = Client::create([
         'id_oficina' => $data['id'],
         'rut' => $data['rut'],
         'digito_verificador' => $data['digito'],
         'nombre' => $data['name'],
         'fecha_nacimiento' => DateTime::createFromFormat('d/m/Y', $data['date']),
         'nacionalidad' => $data['nacionalidad'],
         'telefono_fijo' => $data['fijo'],
         'telefono_movil' => $data['movil'],
         'email' => $data['email'],
         'email_alternativo' => $data['alternativo'],
         'domicilio' => $data['domicilio'],
         'comuna' => $data['comuna'],
         'empresa'=> $data['empresa'],
         'rut_empresa' => $data['rut-empresa'],
         'actividad_empresa' => $data['actividad_empresa'],
         'user_id' => $user->id,
         'dicom' => false,
     ]);
     $service_client = ClientService::create([
         'client_id' => $client->id,
         'service_id' => $data['servicio'],
         'dia_pago' => $data['dia'],
         'monto_pago' => $data['mensualidad'],
         'inscripcion' => $data['inscripcion'],
         'frecuencia'  => $data['frecuencia'],
         'date' => $fecha_pago,
         'fecha_vencimiento' => $fecha_vencimiento,
         'fecha_proxima_cuota' => $fecha_proxima
     ]);
     $payment = Payment::create([
         'client_service_id' => $service_client->id,
         'fecha_pago' => $fecha_pago,
         'total'=> $data['mensualidad']+ $data['inscripcion'],
         'forma_pago' =>$data['forma_pago'],
         'monto_cuota'=> $data['mensualidad'],
         'fecha_vencimiento' => $fecha_pago,
     ]);
     $cuota = Fee::create([
         'client_service_id' => $service_client->id,
         'fecha_pago' => $fecha_pago,
         'fecha_vencimiento' => $fecha_pago,
         'total'=> $data['mensualidad']+ $data['inscripcion'],
         'payment' => true,
         'payment_id' => $payment->id
     ]);
     $cuota_a_pagar = Fee::create([
         'client_service_id' => $service_client->id,
         'fecha_vencimiento' => $fecha_vencimiento,
     ]);
     return Redirect::route('cliente', array('id' => $client->id));
  }
}
