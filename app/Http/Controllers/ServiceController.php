<?php

namespace App\Http\Controllers;
use App\Http\Request;
use Illuminate\Support\Facades\Redirect;

Use App\Service;
Use App\ClientService;
Use App\Fee;
Use App\Payment;

class ServiceController extends Controller
{
  public function showService($id)
  {
     return view('servicios_show', ['service' => Service::findOrFail($id)]);
  }

  public function deleteService($id)
  {
    $service = Service::findOrFail($id);
    $client_services = $service ->clientservices;
    foreach ($client_services as $client) {
      $deletedFees = Fee::where('client_service_id', $client->id)->delete();
      $deletedPayments = Payment::where('client_service_id', $client->id)->delete();
    }
    $deletedServices = ClientService::where('service_id', $id)->delete();
    $service->delete();
    return Redirect::to("/servicios");
  }

  public function createService()
  {
     $this -> validate(request(),[
       'name' => ['required'],
       'description' => ['required']
     ]);
     $data = request()->only(['name','description']);
     Service::create($data);
     return Redirect::to("/servicios");
  }


}
