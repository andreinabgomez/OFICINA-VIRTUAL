<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Client;
Use App\ClientService;
Use App\User;
Use App\Payment;
use DateTime;
use DB;
Use App\UniquePayment;
use Auth;
Use App\Fee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->type == 'Cliente'){
        $client = Client::where('user_id', '=', Auth::user()->id)->get()->first();
        return view('home',compact('client'));
      }
      else{
        $adeudado = 0;
        $today = Carbon::now();
        $clients_vencidos = ClientService::where('fecha_vencimiento', '<', $today)->whereNotNull('fecha_vencimiento')->orderBy('client_id')->get();
        foreach ($clients_vencidos as $client) {
          if ($client->fecha_proxima_cuota < $today && $client->fecha_proxima_cuota != null){
            $cuota = Fee::where('fecha_vencimiento' ,'=', $client->fecha_proxima_cuota )->where('client_service_id' ,'=', $client->id )->count();
            if ($cuota == 0)
            {
              Fee::create([
                  'client_service_id' => $client->id,
                  'fecha_vencimiento' => $client->fecha_proxima_cuota,
              ]);
              $frecuencia = 0;
              if($client->frecuencia == 'Semestral')
               {
                   $frecuencia = 6;
               }
               elseif($client->frecuencia =='Anual')
               {
                   $frecuencia = 12;
               }
               elseif($client->frecuencia =='Trimestral')
               {
                   $frecuencia = 3;
               }
               elseif($client->frecuencia =='Mensual')
               {
                   $frecuencia = 1;
               }
              $fecha_proxima = Carbon::parse($client->fecha_proxima_cuota)->addMonths($frecuencia);
              $client->fecha_proxima_cuota = $fecha_proxima;
              $client->save();
              if ($fecha_proxima > $today){
                Fee::create([
                    'client_service_id' => $client->id,
                    'fecha_vencimiento' => $client->fecha_proxima_cuota,
                ]);
                $fecha_proxima_next = Carbon::parse($client->fecha_proxima_cuota)->addMonths($frecuencia);
                $client->fecha_proxima_cuota = $fecha_proxima_next;
                $client->save();
              }

            }
          }
          foreach( $client->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now()) as $fee ){
            $adeudado = $adeudado + $fee->totales();
          }
        }
        $date = Carbon::now()->subMonth();
        $contratos = ClientService::whereMonth('date', '=', date('m', strtotime($today)))->groupBy('client_id')->select('client_id', DB::raw('count(*) as total'))->get();
        $clients = $contratos->count();
        $recaudado = 0;
        $recaudado_pasado = 0;
        $total = 0;
        $total_pasado = 0;
        $payments = Payment::where('fecha_pago' ,'>', $date)->get();
        foreach($payments as $payment ){
          if(date('m', strtotime($payment->fecha_pago)) == date('m', strtotime($today)))
          {
            $recaudado = $recaudado + $payment->monto_cuota;
            $total = $total + $payment->total;
          }
          if(date('m', strtotime($payment->fecha_pago)) == date('m', strtotime($date)))
          {
            $recaudado_pasado = $recaudado_pasado + $payment->monto_cuota;
            $total_pasado = $total_pasado + $payment->total;
          }
        }
        $pagos_unicos = UniquePayment::where('date' ,'>', $date )->get();
        foreach($pagos_unicos as $payment ){
          if(date('m', strtotime($payment->fecha_pago)) == date('m', strtotime($today)))
          {
            $recaudado = $recaudado + $payment->monto_cuota;
            $total = $total + $payment->total;
          }
          if(date('m', strtotime($payment->fecha_pago)) == date('m', strtotime($date)))
          {
            $recaudado_pasado = $recaudado_pasado + $payment->monto_cuota;
            $total_pasado = $total_pasado + $payment->total;
          }
        }

        return view('home',compact('clients','adeudado','recaudado','total','recaudado_pasado','total_pasado'));
      }
    }
}
