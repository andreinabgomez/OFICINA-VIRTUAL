<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Client;
Use App\Service;
Use App\ClientService;
use DateTime;
Use App\Payment;
Use App\Fee;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class ClientServicesController extends Controller
{

  public function editService($id)
  {
     return view('cliente_servicio_edit', ['service' => ClientService::findOrFail($id)]);
  }

  public function updateService($id)
  {
    $client_service = ClientService::find($id);
    $client = $client_service->client;
    $data = request()->all();
    $client_service->monto_pago = $data['mensualidad'];
    $client_service->frecuencia = $data['frecuencia'];
    if($data['frecuencia'] == 'Semestral')
     {
         $frecuencia = 6;
     }
     elseif($data['frecuencia'] =='Anual')
     {
         $frecuencia = 12;
     }
     elseif($data['frecuencia'] =='Trimestral')
     {
         $frecuencia = 3;
     }
     elseif($data['frecuencia'] =='Mensual')
     {
         $frecuencia = 1;
     }
    $today = Carbon::now();
    $last_fee_paid = Fee::where('client_service_id', $client_service->id)->where('payment', '=', true)->orderby('created_at', 'desc')->first();
    $fecha_pago = $last_fee_paid->fecha_vencimiento;
    if ($client_service ->fecha_vencimiento > $today){
      $day = $client_service ->dia_pago;
      $month = Carbon::parse($fecha_pago)->month+$frecuencia;
      $year = Carbon::parse($fecha_pago)->year;
      $fecha_vencimiento = date('Y-m-d', mktime(0,0,0, $month, $day, $year));
      $fecha_proxima = Carbon::parse($fecha_vencimiento)->addMonths($frecuencia);
      $client_service->fecha_vencimiento = $fecha_vencimiento;
      $client_service->fecha_proxima_cuota = $fecha_proxima;
      $last_fee_unpaid= Fee::where('client_service_id', $client_service->id)->where('payment', '=', false)->orderby('created_at', 'desc')->first();
      $last_fee_unpaid->fecha_vencimiento = $fecha_vencimiento;
      $last_fee_unpaid->save();
    }
    else {
      $last_fee_unpaid = Fee::where('client_service_id', $client_service->id)->where('payment', '=', false)->orderby('created_at', 'desc')->get();
      $fecha = $last_fee_unpaid->last()->fecha_vencimiento;
      $fecha_proxima = Carbon::parse($fecha)->addMonths($frecuencia);
      if($last_fee_unpaid->count() > 1){
        $fee = $last_fee_unpaid[0];
        $fee ->fecha_vencimiento = $fecha_proxima;
        $fee -> save();
      }
      $fecha_proxima_cuota = Carbon::parse($fecha_proxima)->addMonths($frecuencia);
      $client_service->fecha_proxima_cuota = $fecha_proxima_cuota;
    }
    $client_service->save();
    return Redirect::route('cliente', array('id' => $client->id));
  }

  public function newService($id)
  {
     $services = Service::all();
     return view('cliente_servicio_form', ['client' => Client::findOrFail($id)], compact('services'));
  }


  public function deleteService($id)
  {
    $client_service = ClientService::findOrFail($id);
    $cliente = $client_service->client;
    $deletedFees = Fee::where('client_service_id', $client_service->id)->where('payment', '=', false)->delete();
    $client_service->status = true;
    $client_service->save();
    return Redirect::route('cliente', array('id' => $cliente->id));
  }

  public function createService($id)
  {
     $data = request()->all();
     $frecuencia = 0;
     if($data['frecuencia'] == 'Semestral')
      {
          $frecuencia = 6;
      }
      elseif($data['frecuencia'] =='Anual')
      {
          $frecuencia = 12;
      }
      elseif($data['frecuencia'] =='Trimestral')
      {
          $frecuencia = 3;
      }
      elseif($data['frecuencia'] =='Mensual')
      {
          $frecuencia = 1;
      }
     $fecha_pago = Carbon::createFromFormat('d/m/Y', $data['creacion_servicio']);
     $day = $data['dia'];
     $month = $fecha_pago->month+$frecuencia;
     $year = $fecha_pago->year;
     if($data['frecuencia'] != 'Unica')
      {
          $fecha_vencimiento = date('Y-m-d', mktime(0,0,0, $month, $day, $year));
          $fecha_proxima = Carbon::parse($fecha_vencimiento)->addMonths($frecuencia);
      }
      else {
        $fecha_vencimiento = null;
        $fecha_proxima = null;
      }
      $service_client = ClientService::create([
         'client_id' => $id,
         'service_id' => $data['servicio'],
         'dia_pago' => $data['dia'],
         'monto_pago' => $data['mensualidad'],
         'inscripcion' => $data['inscripcion'],
         'frecuencia'  => $data['frecuencia'],
         'date' => $fecha_pago,
         'fecha_vencimiento' => $fecha_vencimiento,
         'fecha_proxima_cuota' => $fecha_proxima
     ]);
     $payment = Payment::create([
         'client_service_id' => $service_client->id,
         'fecha_pago' => $fecha_pago,
         'total'=> $data['mensualidad']+ $data['inscripcion'],
         'forma_pago' =>$data['forma_pago'],
         'monto_cuota'=> $data['mensualidad'],
         'fecha_vencimiento' => $fecha_pago,
     ]);
     $cuota = Fee::create([
         'client_service_id' => $service_client->id,
         'fecha_pago' => $fecha_pago,
         'fecha_vencimiento' => $fecha_pago,
         'total'=> $data['mensualidad']+ $data['inscripcion'],
         'payment' => true,
         'payment_id' => $payment->id,
     ]);
     $cuota_a_pagar = Fee::create([
         'client_service_id' => $service_client->id,
         'fecha_vencimiento' => $fecha_vencimiento,
     ]);
     return Redirect::route('cliente', array('id' => $id));
  }
}
