<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\ClientService;
use Carbon\Carbon;
use Illuminate\Support\Collection;
Use App\Payment;
Use App\Fee;
use Illuminate\Support\Facades\Redirect;

class FeesController extends Controller
{
  public function newPayment($id)
  {
    $client = ClientService::findOrFail($id);
    $cuotas = $client->fees->where('payment', '=', false);
    foreach ($cuotas as $cuota) {
      $cuota->interes = $cuota->intereses();
      $cuota->gasto_cobranza = $cuota->gastos();
      $cuota->total = $cuota->totales();
      $cuota->descuento = 0;
      $cuota->save();
    }
    $monto_descuento = 0;
    $monto_cuotas = $client->monto_cuota();
    $monto_total = $client->total();
    $monto_interes = $client->intereses();
    $monto_gasto = $client->gasto();
    $ids = $cuotas->pluck('id')->toArray();
    return view('pagos_form', ['client' => $client],compact('monto_total','monto_interes','monto_gasto','monto_descuento','monto_cuotas','ids'));
  }

  public function consultarPayment($id)
  {
     $data = request()->all();
     $client = ClientService::findOrFail($id);
     $cuotas = $client->fees->where('payment', '=', false);
     $monto_total = 0;
     $monto_interes = 0;
     $monto_gasto = 0;
     $monto_descuento = 0;
     $monto_cuotas = 0;
     $ids = Collection::make();
     foreach ($cuotas as $cuota) {
       if (request()->has($cuota->id)){
         $interes = "interes_".$cuota->id;
         $gastos = "gasto_".$cuota->id;
         $descuento = "descuento_".$cuota->id;
         $cuota->interes = $data[$interes];
         $cuota->gasto_cobranza = $data[$gastos];
         $cuota->descuento = $data[$descuento];
         $cuota->total = $cuota->interes + $cuota->gasto_cobranza + $client ->monto_pago - $cuota->descuento ;
         $cuota->save();
         $monto_total = $monto_total + $cuota->total;
         $monto_interes = $monto_interes + $cuota->interes;
         $monto_gasto = $monto_gasto + $cuota->gasto_cobranza;
         $monto_descuento = $monto_descuento + $cuota->descuento;
         $monto_cuotas =  $monto_cuotas + $client ->monto_pago;
         $ids->push($cuota->id);
       }
     }
     if ($ids->count()== 0){
       foreach ($cuotas as $cuota) {
         $cuota->interes = $cuota->intereses();
         $cuota->gasto_cobranza = $cuota->gastos();
         $cuota->total = $cuota->totales();
         $cuota->descuento = 0;
         $cuota->save();
       }
       $monto_descuento = 0;
       $monto_cuotas = $client->monto_cuota();
       $monto_total = $client->total();
       $monto_interes = $client->intereses();
       $monto_gasto = $client->gasto();
       $ids = $cuotas->pluck('id')->toArray();
     }
     return view('pagos_form', ['client' => $client],compact('monto_total','monto_interes','monto_gasto','monto_descuento','monto_cuotas','ids'));
  }

  public function makePayment($id)
  {
     $client = ClientService::findOrFail($id);
     $data = request()->all();
     $monto_total = $data['total'];
     $monto_interes = $data['intereses'];
     $monto_gasto = $data['gastos'];
     $monto_descuento = $data['descuentos'];
     $monto_cuotas = $data['cuotas'];
     $fecha_pago = Carbon::createFromFormat('d/m/Y', $data['date']);
     $cuotas = $client->fees->where('payment', '=', false);
     $payment = Payment::create([
         'client_service_id' => $client->id,
         'fecha_pago' => $fecha_pago,
         'total'=> $monto_total,
         'forma_pago' =>$data['forma_pago'],
         'descuento' => $monto_descuento,
         'interes' => $monto_interes,
         'gasto_cobranza' => $monto_gasto,
         'fecha_vencimiento' => $fecha_pago,
         'monto_cuota' => $monto_cuotas,
     ]);
     foreach ($cuotas as $cuota) {
       if (request()->has($cuota->id)){
         $cuota->payment_id = $payment->id;
         $cuota->payment = true;
         $cuota->fecha_pago = $fecha_pago;
         $cuota->save();
       }
     }
     $payment->fecha_vencimiento = $payment->fees->last()->fecha_vencimiento;
     $payment->save();
     if($client->fees->where('payment', '=', false)->count() > 0){
       $cuota = $client->fees->where('payment', '=', false)->first();
       $client->fecha_vencimiento = $cuota->fecha_vencimiento;
       $client->save();
     }
     else{
       $client->fecha_vencimiento = $client->fecha_proxima_cuota;
       $frecuencia = 0;
       if($client->frecuencia == 'Semestral')
        {
            $frecuencia = 6;
        }
        elseif($client->frecuencia =='Anual')
        {
            $frecuencia = 12;
        }
        elseif($client->frecuencia =='Trimestral')
        {
            $frecuencia = 3;
        }
        elseif($client->frecuencia =='Mensual')
        {
            $frecuencia = 1;
        }
       $fecha_proxima = Carbon::parse($client->fecha_proxima_cuota)->addMonths($frecuencia);
       $client->fecha_proxima_cuota = $fecha_proxima;
       $client->save();
       $cuota_a_pagar = Fee::create([
         'client_service_id' => $client->id,
          'fecha_vencimiento' => $client->fecha_vencimiento
       ]);
       $cuota_a_pagar = Fee::create([
         'client_service_id' => $client->id,
          'fecha_vencimiento' => $client->fecha_proxima_cuota
       ]);
     }
     return Redirect::route('cliente', array('id' => $client->client->id));
  }

  public function showFees($id)
  {
     return view('pagos_show', ['payment' => Payment::findOrFail($id)]);
  }

}
