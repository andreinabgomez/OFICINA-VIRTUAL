<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\UniquePayment;
use DateTime;
use Illuminate\Support\Facades\Redirect;

class UniquePaymentsController extends Controller
{
  public function indexPago()
  {
    $clients = UniquePayment::paginate(50);
    return view('pagos_unicos',compact('clients'));
  }

  public function createPago()
  {
     $data = request()->all();
     $pago = UniquePayment::create([
         'rut' => $data['rut'],
         'digito_verificador' => $data['digito'],
         'nombre' => $data['nombre'],
         'date' => DateTime::createFromFormat('d/m/Y', $data['date']),
         'telefono' => $data['telefono'],
         'email' => $data['email'],
         'monto_pago' => $data['monto_pago'],
         'service_id' => $data['servicio'],
          'forma_pago' =>$data['forma_pago']
     ]);
     return Redirect::to("/pagos_unicos");
  }
}
