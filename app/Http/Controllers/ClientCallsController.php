<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\ClientCall;
Use App\Client;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class ClientCallsController extends Controller
{
  public function newCall($id)
  {
     return view('bitacora_form', ['client' => Client::findOrFail($id)]);
  }

  public function createCall($id)
  {
     $data = request()->all();
     ClientCall::create([
        'resultado' => $data['resultado'],
        'client_id' => $id,
        'date' => Carbon::now()
      ]);
     return Redirect::route('cliente', array('id' => $id));
  }
}
