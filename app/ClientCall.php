<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientCall extends Model
{
  protected $fillable = [
      'resultado', 'client_id','date'
  ];
}
