<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DateTime;
use Carbon\Carbon;
Use App\Information;

class ClientService extends Model
{
  protected $fillable = [
    'client_id','dia_pago','frecuencia','monto_pago','inscripcion','date','service_id','fecha_vencimiento','fecha_proxima_cuota'
  ];

  public function service()
  {
      return $this->belongsTo('App\Service');
  }

  public function fees()
  {
    return $this->hasMany('App\Fee');
  }

  public function payments()
  {
    return $this->hasMany('App\Payment')->orderBy('fecha_pago','desc');
  }

  public function client()
  {
      return $this->belongsTo('App\Client');
  }

  public function status()
  {
    $date = Carbon::now();

    if($this->fecha_vencimiento < $date) {
      if($this->fecha_vencimiento == null){
        return 'AL DIA';
      }
      else {
        $date_vencida = Carbon::parse($this->fecha_vencimiento);
        return $date_vencida->diffInDays($date).' DIAS VENCIDO';
      }
    }
    elseif($this->fecha_vencimiento < ($date->addWeeks(3))){
      return 'PROXIMO VENCER';
    }
    else{
      return 'AL DIA';
    }
  }

  public function fecha_mostrada()
  {
    $date = Carbon::now();
    if($this->fecha_vencimiento > $date) {
      return $this->fecha_vencimiento;
    }
    else {
      return $this->fecha_proxima_cuota;
    }
  }

  public function status_css()
  {
    $date = Carbon::now();
    if($this->fecha_vencimiento < $date) {
      if($this->fecha_vencimiento == null){
        return 'solvente';
      }
      else {
        $date_vencida = Carbon::parse($this->fecha_vencimiento);
        return 'vencido';
      }
    }
    elseif($this->fecha_vencimiento < ($date->addWeeks(3))){
      return 'proximo_vencer';
    }
    else{
      return 'solvente';
    }
  }

  public function dias_vencidos()
  {
    $date = Carbon::now();
    $date_vencida = Carbon::parse($this->fecha_vencimiento);
    return $date_vencida->diffInDays($date);
  }

  public function intereses()
  {
    $cuotas = $this->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now());
    $intereses = 0;
    foreach ($cuotas as $cuota) {
      $intereses = $intereses + $cuota->intereses();
    }
    return $intereses;
  }

  public function gasto()
  {
    $cuotas = $this->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now());
    $gasto = 0;
    foreach ($cuotas as $cuota) {
      $gasto = $gasto + $cuota->gastos();
    }
    return $gasto;
  }

  public function total()
  {
    $cuotas = $this->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now());
    $monto = 0;
    foreach ($cuotas as $cuota) {
      $monto = $monto + $cuota->totales();
    }
    if ($monto > 0){
      return $monto;
    }
    else {
      return $this->monto_pago;
    }
  }

  public function monto_cuota()
  {
    $cuotas = $this->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now());
    $monto = 0;
    $monto = 0;
    foreach ($cuotas as $cuota) {
      $monto = $monto + $this->monto_pago;
    }
    return $monto;
  }

  public function anterior()
  {
    $cuotas = $this->fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon::now());
    $monto = 0;
    foreach ($cuotas as $cuota) {
      $monto = $monto + $cuota->totales();
    }
    return $monto;
  }

}
