<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UniquePayment extends Model
{
  protected $fillable = [
    'date','service_id','nombre','monto_pago','rut','digito_verificador','telefono','email','forma_pago'
  ];

  public function service()
  {
      return $this->belongsTo('App\Service');
  }

}
