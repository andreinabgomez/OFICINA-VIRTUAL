<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Use App\Service;
Use App\Client;
Use App\ClientService;
use Carbon\Carbon;

Auth::routes();

Route::get('/',function(){
  if(Auth::guest()) {
    return view('auth/login');
  }
  else {
    return redirect('/home');
  }
});

Route::get('/servicios', function(){
  $services = Service::all();
  return view('servicios_index', compact('services'));
});

Route::get('/servicios/new', function(){
  return view('servicios_form');
});

Route::get('/pagos_unicos/new', function(){
    $services = Service::all();
  return view('pago_unico_form',  compact('services'));
});

Route::get('/usuarios/new', function(){
  return view('usuario_form');
});

Route::get('/clientes/new', function(){
  $services = Service::all();
  return view('clientes_form',compact('services'));
});

Route::get('/cuenta/{id}', function($id){
  return view('cuenta');
})->where('id', '[0-9]+');

Route::get('/clientes/{id}/new_pago', 'FeesController@newPayment')->where('id', '[0-9]+');
Route::post('/clientes/{id}/new_pago', 'FeesController@consultarPayment')->where('id', '[0-9]+');
Route::post('/clientes/{id}/new_pago/payment', 'FeesController@makePayment')->where('id', '[0-9]+');

Route::get('/home', 'HomeController@index');

Route::post('/servicios/new', 'ServiceController@createService');

Route::post('/pagos_unicos/new', 'UniquePaymentsController@createPago');

Route::get('/clientes', 'ClientsController@index');

Route::get('servicios/{id}', 'ServiceController@showService')->where('id', '[0-9]+');

Route::get('/clientes/{id}/new_bitacora', 'ClientCallsController@newCall')->where('id', '[0-9]+');

Route::post('/clientes/{id}/new_bitacora', 'ClientCallsController@createCall')->where('id', '[0-9]+');

Route::get('/clientes/{id}/bitacora', 'ClientsController@showCalls')->where('id', '[0-9]+');

Route::get('/clientes/{id}/edit', 'ClientsController@editClient')->where('id', '[0-9]+');

Route::post('/clientes/{id}/edit', 'ClientsController@updateClient')->where('id', '[0-9]+');

Route::post('/clientes_servicios/{id}/edit', 'ClientServicesController@updateService')->where('id', '[0-9]+');

Route::get('/clientes_servicios/{id}/edit', 'ClientServicesController@editService')->where('id', '[0-9]+');

Route::get('/clientes/{id}/pagos', 'ClientsController@showPayments')->where('id', '[0-9]+');

Route::get('/clientes/{id}/new_servicio', 'ClientServicesController@newService')->where('id', '[0-9]+');

Route::get('/eliminar_cliente/{id}', 'ClientsController@deleteClient')->where('id', '[0-9]+');

Route::get('/eliminar_user/{id}', 'ClientsController@deleteUser')->where('id', '[0-9]+');

Route::get('/eliminar_servicio/{id}', 'ServiceController@deleteService')->where('id', '[0-9]+');

Route::get('/eliminar_cliente_servicio/{id}', 'ClientServicesController@deleteService')->where('id', '[0-9]+');

Route::post('/clientes/{id}/new_servicio', 'ClientServicesController@createService')->where('id', '[0-9]+');

Route::get('/clientes_vencidos', 'ClientsController@indexVencidos');

Route::get('/clientes_proximos_vencidos','ClientsController@indexProximos' );

Route::get('/clientes_eliminar','ClientsController@indexEliminar');

Route::post('/clientes/new', 'ClientsController@createClient');

Route::get('/bitacoras', 'ClientsController@indexBitacora');

Route::get('/nuevos_contratos', 'ClientsController@indexContrato');

Route::get('/usuarios', 'ClientsController@indexUsuarios');

Route::get('/pagos', 'ClientsController@indexPago');

Route::get('/pagos_unicos', 'UniquePaymentsController@indexPago');

Route::get('/payment/{id}', 'FeesController@showFees')->where('id', '[0-9]+')->name('payment');

Route::get('/clientes/{id}', 'ClientsController@showClient')->where('id', '[0-9]+')->name('cliente');

Route::get('/clientes/{id}/dicom', 'ClientsController@registrarDicom')->where('id', '[0-9]+');

Route::get('/clientes/{id}/dicomoff', 'ClientsController@eliminarDicom')->where('id', '[0-9]+');
