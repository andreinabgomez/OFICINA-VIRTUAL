@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-9">
      <h4>CLIENTES PAGOS</h4>
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Fecha Pago</th>
          <th>ID</th>
          <th>Nombre</th>
          <th>Servicio</th>
          <th>Monto</th>
          <th>Intereses</th>
          <th>Gastos Cobranza</th>
          <th>Descuento</th>
          <th>Monto Cancelado</th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $payment)
      <tr>
        <td>{{Carbon\Carbon::parse($payment -> fecha_pago)->format('d-m-Y')}}</td>
        <td>{{$payment->client_service->client->id_oficina}}</td>
        <td>{{$payment->client_service->client->nombre}}</td>
        <td>{{$payment->client_service->service->name}}</td>
        <td>${{number_format($payment -> monto_cuota, 0, ",", ".")}}</td>
        <td>${{number_format($payment -> interes, 0, ",", ".")}}</td>
        <td>${{number_format($payment -> gasto_cobranza, 0, ",", ".")}}</td>
        <td>${{number_format($payment -> descuento, 0, ",", ".")}}</td>
        <td>${{number_format($payment -> total, 0, ",", ".")}}</td>
        <td>
          <a href="{{ action('FeesController@showFees',  array('id' => $payment->id)) }}">
            <i class="fa fa-arrow-right"></i>
          </a>
        </td>
      </tr>
      @endforeach

    </tbody>
  </table>
  @include('pagination.default', ['paginator' => $clients])
</div>
@endsection
