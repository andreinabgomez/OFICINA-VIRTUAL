@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-10">
      <h4>SERVICIOS</h4>
    </div>
    <div class ="col-md-2 div-boton">
      <a class="btn-border-blue" href="/servicios/new">NUEVO SERVICIO</a>
    </div>
  </div>
  <table class="table table-list">
    <thead>
      <tr>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th></th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($services as $service)
        <tr>
            <td>{{$service -> name}}</td>
            <td>{{ str_limit($service -> description, $limit = 130, $end = '...') }}</td>
            <td>
              <a href="{{ action('ServiceController@deleteService',  array('id' => $service -> id)) }}">
                <i class="fa fa-trash"></i>
              </a>
            </td>
            <td>
              <a href="{{ action('ServiceController@showService',  array('id' => $service -> id)) }}">
                <i class="fa fa-arrow-right"></i>
              </a>
            </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection
