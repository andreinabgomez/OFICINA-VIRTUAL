@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Editar Cliente - {{$client -> id_oficina}} - {{$client -> nombre}}</div>
      <div class="panel-body">
        <form class="form-horizontal" role="form" method="POST">
             {{ csrf_field() }}
              <div class="col-md-12 titulo-col">
                <span class="precio titulo">INFORMACION PERSONAL</span>
              </div>
              <div class="col-md-2">
                <input id="id" name='id' type="text" class="form-control" required autofocus placeholder="ID" value="{{$client -> id_oficina}}">
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-md-2">
                <input type="text" name='rut' class="form-control" required autofocus placeholder="RUT" value="{{$client -> rut}}">
              </div>
              <div class="col-md-1">
                <input  type="text" name='digito' class="form-control" required autofocus value="{{$client -> digito_verificador}}">
              </div>
              <div class="col-md-3">
                  <input type="text" name='name' class="form-control" required autofocus placeholder="Nombre" value="{{$client -> nombre}}">
              </div>
              <div class="col-md-3">
                <div id = 'calendar' class="input-group date">
                  <input type="text" required  name='date' placeholder="Fecha Nacimiento" value="{{Carbon\Carbon::parse($client -> fecha_nacimiento)->format('d/m/Y')}}" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                </div>
              </div>
              <div class="col-md-3 bottom">
                  <input name='nacionalidad' type="text" class="form-control" autofocus placeholder="Nacionalidad" value="{{$client -> nacionalidad}}">
              </div>
              <div class="col-md-3 bottom">
                  <input  name='fijo' type="text" class="form-control" autofocus placeholder="Tlf Fijo" value="{{$client -> telefono_fijo}}">
              </div>
              <div class="col-md-3 bottom">
                  <input name='movil' type="text" class="form-control" required autofocus placeholder="Tlf Movil" value="{{$client -> telefono_movil}}">
              </div>
              <div class="col-md-3 bottom">
                  <input name='email' type="text" class="form-control" required autofocus placeholder="Email" value="{{$client -> email}}">
              </div>
              <div class="col-md-3 bottom">
                  <input name='alternativo' type="text" class="form-control" autofocus placeholder="Email Alternativo" value="{{$client -> email_alternativo}}">
              </div>
              <div class="col-md-6 bottom">
                  <input name='domicilio' type="text" class="form-control" autofocus placeholder="Domicilio" value="{{$client -> domicilio}}">
              </div>
              <div class="col-md-3 bottom">
                  <input name='comuna' type="text" class="form-control" autofocus placeholder="Comuna" value="{{$client -> comuna}}">
              </div>
              <div class="col-md-12">
                <hr>
                <span class="precio titulo">INFORMACION DE EMPRESA</span>
              </div>
              <div class="col-md-3 bottom">
                  <input name='empresa' type="text" class="form-control" autofocus placeholder="Empresa" value="{{$client -> empresa}}">
              </div>
              <div class="col-md-3 bottom">
                  <input name='rut-empresa' type="text" class="form-control" autofocus placeholder="RUT" value="{{$client -> rut_empresa}}">
              </div>
              <div class="col-md-6 bottom">
                  <input name='actividad_empresa' type="text" class="form-control" autofocus placeholder="Actividad" value="{{$client -> actividad_empresa}}">
              </div>
              <div class="form-group">
                  <div class="col-md-12">
                    <hr>
                  </div>
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>
</div>
@endsection
