@extends('layouts.app')

@section('content')
<div class="container">
  <div id="breadcrumbs" class="breadcrumbs">
    <a href="/bitacoras">
      < BITACORAS
    </a>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">BITACORA DE LLAMADAS - {{$client -> id_oficina}} - {{$client -> nombre}}</div>

      <div class="panel-body">
          <table class="table table-list">
            <div class="col-md-10 titulo-col">

            </div>
            <div class ="col-md-2 div-boton">
              <a class="btn-border-blue" href="{{ action('ClientCallsController@newCall',  array('id' => $client -> id)) }}">NUEVA BITACORA</a>
            </div>
            <thead>
              <tr>
                  <th class='td-date'>Fecha</th>
                  <th>Resultado</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($client->calls as $call)
              <tr>
                  <td>{{Carbon\Carbon::parse($call->date)->format('d-m-Y')}}</td>
                  <td>{{$call -> resultado}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

      </div>
  </div>

</div>
@endsection
