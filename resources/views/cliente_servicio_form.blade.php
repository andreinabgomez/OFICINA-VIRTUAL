@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Nuevo Servicio - {{$client -> id_oficina}} - {{$client -> nombre}}</div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST">
            {{ csrf_field() }}
              <div class="col-md-3">
                <select name='servicio'  class="form-control">
                  <option value="" selected>Selecciona el Plan</option>
                  @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3">
                <select name='dia'  class="form-control">
                  <option value="" selected>Selecciona el dia de pago</option>
                  <option value="05">05</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="25">25</option>
                  <option value="30">30</option>
                </select>
              </div>
              <div class="col-md-3">
                <select name='frecuencia' class="form-control">
                  <option value="" selected>Selecciona el tipo de pago</option>
                  <option value="Semestral">Semestral</option>
                  <option value="Anual">Anual</option>
                  <option value="Trimestral">Trimestral</option>
                  <option value="Mensual">Mensual</option>
                  <option value="Unica">Una vez</option>
                </select>
              </div>
              <div class="col-md-3">
                <select name='forma_pago' class="form-control">
                  <option value="" selected>Selecciona la forma del primer pago</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="Tarjeta">Tarjeta</option>
                  <option value="Deposito">Deposito</option>
                </select>
              </div>
              <div class="col-md-3 bottom">
                  <input name='mensualidad' type="text" class="form-control" required autofocus placeholder="Monto a Pagar">
              </div>
              <div class="col-md-3 bottom">
                  <input name='inscripcion' type="text" class="form-control" required autofocus placeholder="Inscripcion">
              </div>
              <div class="col-md-3 bottom">
                <div id = 'calendar_1' class="input-group date">
                  <input type="text" name='creacion_servicio' placeholder="Fecha primer pago" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                </div>
              </div>

              <div class="form-group">
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
