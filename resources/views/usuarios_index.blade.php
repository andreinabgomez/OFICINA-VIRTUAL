@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-10">
      <h4>USUARIOS</h4>
    </div>
    <div class ="col-md-2 div-boton">
      <a class="btn-border-blue" href="{{ route('register') }}">NUEVO USUARIO</a>
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Nombre</th>
          <th>Email</th>
          <th>Tipo</th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $client)
        <tr>
            <td>{{$client -> name}}</td>
            <td>{{$client -> email}}</td>
            <td>{{$client -> type}}</td>
            <td>
              <a href="{{ action('ClientsController@deleteUser',  array('id' => $client -> id)) }}">
                <i class="fa fa-trash"></i>
              </a>
            </td>
        </tr>
      @endforeach

    </tbody>
  </table>
</div>
@endsection
