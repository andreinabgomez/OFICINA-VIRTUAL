<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Oficina Virtual Chile') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link href="{{ asset('css/ace.min.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script src="{{ asset('js/sorttable.js') }}"></script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Branding Image -->
                    <img class='header_imagen' SRC="/logotipooficinavirtualchile.png">
                    <a class="navbar-brand nav-bar-tittle" href="{{ url('/login') }}">
                        {{ config('app.name', 'Oficina Virtual Chile') }}
                    </a>

                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </a>
                            </li>
                        @endif
                    </ul>

                </div>
            </div>
        </nav>
        @if (Auth::guest())

        @else
          @if (Auth::user()->type == 'Empleado')
            <nav class="navbar navbar-default menu-nav primer-menu">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                  <ul class="nav navbar-nav">
                    <li><a href="/clientes">Clientes</a></li>
                    <li><a href="/servicios">Servicios</a></li>
                    <li><a href="/clientes_vencidos">Clientes Vencidos</a></li>
                    <li><a href="/clientes_proximos_vencidos">Clientes Proximos a vencer</a></li>
                    <li><a href="/bitacoras">Bitacora de llamadas</a></li>
                    <li><a href="/pagos">Pagos de clientes</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  </ul>
              </div><!-- /.container-fluid -->
            </nav>
            <nav class="navbar navbar-default menu-nav">
              <div class="container-fluid search-menu">
                <div class='col-md-6 search-div'>


                </div>

                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="search" placeholder="Buscar Cliente.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'id'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="id" placeholder="Buscar por ID.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div><!-- /.container-fluid -->
            </nav>
          @endif
          @if (Auth::user()->type == 'Cobranza')
            <nav class="navbar navbar-default menu-nav primer-menu">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                  <ul class="nav navbar-nav">
                    <li><a href="/clientes">Clientes</a></li>
                    <li><a href="/clientes_vencidos">Clientes Vencidos</a></li>
                    <li><a href="/clientes_proximos_vencidos">Clientes Proximos a vencer</a></li>
                    <li><a href="/bitacoras">Bitacora de llamadas</a></li>
                    <li><a href="/pagos">Pagos de clientes</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  </ul>
              </div><!-- /.container-fluid -->
            </nav>
            <nav class="navbar navbar-default menu-nav">
              <div class="container-fluid search-menu">
                <div class='col-md-6 search-div'>


                </div>

                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="search" placeholder="Buscar Cliente.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'id'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="id" placeholder="Buscar por ID.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div><!-- /.container-fluid -->
            </nav>
          @endif
          @if (Auth::user()->type == 'Administrador')
            <nav class="navbar navbar-default menu-nav primer-menu">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                  <ul class="nav navbar-nav">
                    <li><a href="/clientes">Clientes</a></li>
                    <li><a href="/servicios">Servicios</a></li>
                    <li><a href="/clientes_vencidos">Clientes Vencidos</a></li>
                    <li><a href="/clientes_proximos_vencidos">Clientes Proximos a vencer</a></li>
                    <li><a href="/bitacoras">Bitacora de llamadas</a></li>
                    <li><a href="/clientes_eliminar">Eliminar Clientes</a></li>
                    <li><a href="{{ action('ClientsController@indexUsuarios') }}">Agregar Usuarios</a></li>
                    <li><a href="/pagos">Pagos de clientes</a></li>
                    <li><a href="/pagos_unicos">Pagos Unicos</a></li>
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  </ul>
              </div><!-- /.container-fluid -->
            </nav>
            <nav class="navbar navbar-default menu-nav">
              <div class="container-fluid search-menu">
                <div class='col-md-6 search-div'>


                </div>

                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="search" placeholder="Buscar Cliente.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
                <div class='col-md-3 search-div'>
                  {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'id'])  !!}

                    <div class="input-group custom-search-form">
                        <input type="search" name="id" placeholder="Buscar por ID.." class='form-control'>
                        <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit">
                              <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                  {!! Form::close() !!}

                </div>
              </div><!-- /.container-fluid -->
            </nav>
          @endif
          @if (Auth::user()->type == 'Cliente')
            <nav class="navbar navbar-default menu-nav">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <!-- Collect the nav links, forms, and other content for toggling -->
                  <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                  </ul>
              </div><!-- /.container-fluid -->
            </nav>
          @endif
        @endif

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment-with-locales.min.js"></script>
    <script src="{{ asset('datePicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>
    <script type="text/javascript">
      $(document).ready(function() {
          $('#calendar').datepicker({
            language: "es",
            autoclose: true
          });
      } );
      $(document).ready(function() {
          $('#calendar_1').datepicker({
            language: "es",
            autoclose: true
          });
      } );
  </script>
</body>
</html>
