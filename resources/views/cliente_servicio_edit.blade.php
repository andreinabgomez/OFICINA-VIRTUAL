@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Edit Servicio - {{$service->client-> id_oficina}} - {{$service->client-> nombre}} - {{$service->service-> name}}</div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST">
            {{ csrf_field() }}
              <div class="col-md-3">
                {{ Form::select('frecuencia', ['' => 'Selecciona el tipo de pago',
                    'Semestral' => 'Semestral',
                    'Anual' => 'Anual', 'Trimestral' => 'Trimestral',
                    'Mensual' => 'Mensual', 'Unica' => 'Unica'], $service->frecuencia, ['class' => 'form-control']
                    ) }}
              </div>
              <div class="col-md-3">
                  <input name='mensualidad' type="text" class="form-control" required autofocus placeholder="Monto a Pagar" value = '{{$service -> monto_pago}}'>
              </div>
              <div class="form-group">
                <div class="col-md-12">
                  <hr>
                </div>
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
