@extends('layouts.app')

@section('content')
  @if (Auth::user()->type == 'Cliente')
  <div class="container">
    <div class="alert alert-block alert-success">
      <button type="button" class="close" data-dismiss="alert">
        <i class="ace-icon fa fa-times"></i>
      </button>

      <i class="ace-icon fa fa-check green"></i>

      Bienvenido a
      <strong class="green">
        OFICINA VIRTUAL CHILE
      </strong>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">INFORMACION</div>

        <div class="panel-body">
            <div class="col-md-12 titulo-col">
              <span class="precio titulo">PERSONAL:</span>
            </div>
            <div class="col-md-4">
              <p>
                <span class="precio">ID:</span>
                {{$client -> id_oficina}}
              </p>
              <p>
                <span class="precio">RUT:</span>
                {{$client -> rut}}-{{$client -> digito_verificador}}
              </p>
              <p>
                <span class="precio">Nombre:</span>
                {{$client -> nombre}}
              </p>
              <p>
                <span class="precio">Fecha Nacimiento:</span>
                {{Carbon\Carbon::parse($client->fecha_nacimiento)->format('d-m-Y')}}
              </p>
              <p>
                <span class="precio">Nacionalidad:</span>
                {{$client -> nacionalidad}}
              </p>
            </div>
            <div class="col-md-4 centro">
              <p>
                <span class="precio">Tlf Fijo:</span>
                {{$client -> telefono_fijo}}
              </p>
              <p>
                <span class="precio">Tlf Movil:</span>
                {{$client -> telefono_movil}}
              </p>
              <p>
                <span class="precio">Email:</span>
                {{$client -> email}}
              </p>
              <p>
                <span class="precio">Email Alternativo:</span>
                {{$client -> email_alternativo}}
              </p>
              <p>
                <span class="precio">Domicilio:</span>
                {{$client -> domicilio}}
              </p>
              <p>
                <span class="precio">Comuna:</span>
                {{$client -> comuna}}
              </p>
            </div>
            <div class="col-md-4">
              <p>
                <span class="precio">Empresa:</span>
                {{$client -> empresa}}
              </p>
              <p>
                <span class="precio">RUT Empresa:</span>
                {{$client -> rut_empresa}}
              </p>
              <p>
                <span class="precio">Actividad Empresa:</span>
                {{$client -> actividad_empresa}}
              </p>
              <p>
                @if ($client -> dicom==true)
                  <span class="dicom">Registrado en DICOM</span>
                @endif
              </p>
            </div>
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-10 titulo-col">
              <span class="precio titulo">SERVICIOS</span>
            </div>
            <table class="table table-list">
              <thead>
                <tr>
                    <th>Plan</th>
                    <th>Tipo Pago</th>
                    <th>Monto Cuota</th>
                    <th>Fecha Vencimiento</th>
                    <th>Cuotas Vencidas</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($client->clientservices as $client_service)
                  <tr>
                      <td>{{$client_service -> service -> name}}</td>
                      <td>{{$client_service -> frecuencia}}</td>
                      <td>${{number_format($client_service -> monto_pago, 0, ",", ".")}}</td>
                      <td>{{Carbon\Carbon::parse($client_service -> fecha_vencimiento)->format('d-m-Y')}}</td>
                      <td>{{$client_service -> fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon\Carbon::now()) ->count()}}</td>
                  </tr>
                @endforeach
              </tbody>
            </table>
            <div class="col-md-12">
              <hr>
            </div>
            <div class="col-md-10 titulo-col">
              <span class="precio titulo">PAGOS</span>
            </div>
            <table class="table table-list">
              <thead>
                <tr>
                    <th>Fecha Pago</th>
                    <th>Vencimiento</th>
                    <th>Servicio</th>
                    <th>Tipo Pago</th>
                    <th>Monto</th>
                    <th>Intereses</th>
                    <th>Gastos Cobranza</th>
                    <th>Descuento</th>
                    <th>Monto Cancelado</th>
                    <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($client->clientservices as $client_service)
                  @foreach ($client_service->payments->take(3) as $payment)
                  <tr>
                    <td>{{Carbon\Carbon::parse($payment -> fecha_pago)->format('d-m-Y')}}</td>
                    <td>{{Carbon\Carbon::parse($payment -> fecha_vencimiento)->format('d-m-Y')}}</td>
                    <td>{{$client_service -> service -> name}}</td>
                    <td>{{$payment -> forma_pago}}</td>
                    <td>${{number_format($payment -> monto_cuota, 0, ",", ".")}}</td>
                    <td>${{number_format($payment -> interes, 0, ",", ".")}}</td>
                    <td>${{number_format($payment -> gasto_cobranza, 0, ",", ".")}}</td>
                    <td>${{number_format($payment -> descuento, 0, ",", ".")}}</td>
                    <td>${{number_format($payment -> total, 0, ",", ".")}}</td>
                    <td>
                      <a href="{{ action('FeesController@showFees',  array('id' => $payment->id)) }}">
                        <i class="fa fa-arrow-right"></i>
                      </a>
                    </td>
                  </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
        </div>
    </div>

  </div>

  @else
    <div class="container">
      <div class="alert alert-block alert-success">
        <button type="button" class="close" data-dismiss="alert">
          <i class="ace-icon fa fa-times"></i>
        </button>

        <i class="ace-icon fa fa-check green"></i>

        Bienvenido a
        <strong class="green">
          OFICINA VIRTUAL CHILE
        </strong>
      </div>
      <div class="space-6"></div>
      <div class="col-sm-12 infobox-container">
        @if (Auth::user()->type == 'Administrador')
          <div class="infobox infobox-green">
            <div class="infobox-icon">
              <i class="ace-icon fa fa-usd" aria-hidden="true"></i>
            </div>

            <div class="infobox-data">
              <span class="infobox-data-number">${{number_format($total, 0, ",", ".")}}</span>
              <div class="infobox-content">Recaudado Total</div>
            </div>
          </div>
        @endif

        <div class="infobox infobox-green">
          <div class="infobox-icon">
            <i class="ace-icon fa fa-usd" aria-hidden="true"></i>
          </div>

          <div class="infobox-data">
            <span class="infobox-data-number">${{number_format($recaudado, 0, ",", ".")}}</span>
            <div class="infobox-content">Recaudado en el mes</div>
          </div>
        </div>

        <div class="infobox infobox-blue">
          <div class="infobox-icon">
            <i class="ace-icon fa fa-file-text-o"></i>
          </div>

          <div class="infobox-data">
            <span class="infobox-data-number">{{$clients}}</span>
            <div class="infobox-content"><a href="{{ action('ClientsController@indexContrato') }}"> Nuevos Contratos
            </a></div>
          </div>
        </div>

        <div class="infobox infobox-red">
          <div class="infobox-icon">
            <i class="ace-icon fa fa-exclamation-triangle"></i>
          </div>

          <div class="infobox-data">
            <span class="infobox-data-number">${{number_format($adeudado, 0, ",", ".")}}</span>
            <div class="infobox-content">Deuda</div>
          </div>
        </div>

        </div>
        <div class="space-6"></div>
        <div class="space-6"></div>
        <div class="col-sm-12 infobox-container">
          @if (Auth::user()->type == 'Administrador')
            <div class="infobox infobox-green">
              <div class="infobox-icon">
                <i class="ace-icon fa fa-usd" aria-hidden="true"></i>
              </div>

              <div class="infobox-data">
                <span class="infobox-data-number">${{number_format($total_pasado, 0, ",", ".")}}</span>
                <div class="infobox-content">Total Mes Pasado</div>
              </div>
            </div>
          @endif

          <div class="infobox infobox-green">
            <div class="infobox-icon">
              <i class="ace-icon fa fa-usd" aria-hidden="true"></i>
            </div>

            <div class="infobox-data">
              <span class="infobox-data-number">${{number_format($recaudado_pasado, 0, ",", ".")}}</span>
              <div class="infobox-content">Recaudado mes pasado</div>
            </div>
          </div>
          </div>
      </div>
    </div>
  @endif
@endsection
