@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Nuevo Pago</div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST">
            {{ csrf_field() }}
              <div class="col-md-3">
                  <input name='nombre' type="text" class="form-control" required autofocus placeholder="Nombre">
              </div>
              <div class="col-md-2">
                <input type="text" name='rut' class="form-control" required autofocus placeholder="RUT">
              </div>
              <div class="col-md-1">
                <input  type="text" name='digito' class="form-control" required autofocus>
              </div>
              <div class="col-md-3">
                  <input name='email' type="text" class="form-control" autofocus placeholder="Email">
              </div>
              <div class="col-md-3">
                  <input name='telefono' type="text" class="form-control" autofocus placeholder="Telefono">
              </div>
              <div class="col-md-3 bottom">
                <select name='servicio' required class="form-control">
                  <option value="" selected>Selecciona el Plan</option>
                  @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3 bottom">
                <select name='forma_pago' required class="form-control">
                  <option value="" selected>Forma de pago</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="Tarjeta">Tarjeta</option>
                  <option value="Deposito">Deposito</option>
                </select>
              </div>
              <div class="col-md-3 bottom">
                  <input name='monto_pago' required type="text" class="form-control" required autofocus placeholder="Monto">
              </div>
              <div class="col-md-3 bottom">
                <div id = 'calendar_1' class="input-group date">
                  <input type="text" name='date' required placeholder="Fecha Pago" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                </div>
                <br>
              </div>

              <div class="form-group">
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
