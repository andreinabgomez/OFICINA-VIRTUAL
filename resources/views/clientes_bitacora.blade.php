@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-9">
      <h4>CLIENTES</h4>
    </div>
    <div class='col-md-3 search-div'>
      {!! Form::open(['method'=>'GET','url'=>'bitacoras','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

        <div class="input-group custom-search-form search_eliminar">
            <input type="search" name="search" placeholder="Buscar.." class='form-control'>
            <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Servicios Vencidos<th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $client)
        <tr>
            <td>{{$client -> id_oficina}}</td>
            <td>{{$client -> nombre}}</td>
            <td>@foreach ($client->clientservices->where('fecha_vencimiento', '<', Carbon\Carbon::now()) as $client_service)
                {{$client_service -> service -> name}},
                @endforeach
                @if ($client->clientservicesvencidos() == 0)
                    No tiene servicios vencidos
                @endif
            </td>
            <td>{{$client -> dias_vencido()}}</td>
            <td>
              <a href="{{ action('ClientsController@showCalls',  array('id' => $client -> id)) }}">
                <i class="fa fa-arrow-right"></i>
              </a>
            </td>
        </tr>
      @endforeach

    </tbody>
  </table>
  @include('pagination.default', ['paginator' => $clients])
</div>
@endsection
