@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-9">
      <h4>NUEVOS CONTRATOS</h4>
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Servicios Nuevos<th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $client)
        <tr>
            <td>{{App\Client::findOrFail($client ->client_id) -> id_oficina}}</td>
            <td>{{App\Client::findOrFail($client ->client_id) -> nombre}}</td>
            <td>@foreach (App\Client::findOrFail($client ->client_id) -> clientservices ->where('date', '>', Carbon\Carbon::now()->subMonth()) as $client_service)
                {{$client_service -> service -> name}},
                @endforeach
            </td>
            <td>
              <a href="{{ action('ClientsController@showClient',  array('id' => $client ->client_id)) }}">
                <i class="fa fa-arrow-right"></i>
              </a>
            </td>
        </tr>
      @endforeach

    </tbody>
  </table>
</div>
@endsection
