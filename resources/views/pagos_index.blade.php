@extends('layouts.app')

@section('content')
<div class="container">
  <div id="breadcrumbs" class="breadcrumbs">
    <a href="/pagos">
      < PAGOS
    </a>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">PAGOS CLIENTE - {{$client -> id_oficina}} - {{$client -> nombre}}</div>

      <div class="panel-body">
        <table class="table table-list">
          <thead>
            <tr>
                <th>Fecha Pago</th>
                <th>Vencimiento</th>
                <th>Servicio</th>
                <th>Tipo Pago</th>
                <th>Monto</th>
                <th>Intereses</th>
                <th>Gastos Cobranza</th>
                <th>Descuento</th>
                <th>Monto Cancelado</th>
                <th></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($client->clientservices as $client_service)
              @foreach ($client_service->payments as $payment)
              <tr>
                <td>{{Carbon\Carbon::parse($payment -> fecha_pago)->format('d-m-Y')}}</td>
                <td>{{Carbon\Carbon::parse($payment -> fecha_vencimiento)->format('d-m-Y')}}</td>
                <td>{{$client_service -> service -> name}}</td>
                <td>{{$payment -> forma_pago}}</td>
                <td>${{number_format($payment -> monto_cuota, 0, ",", ".")}}</td>
                <td>${{number_format($payment -> interes, 0, ",", ".")}}</td>
                <td>${{number_format($payment -> gasto_cobranza, 0, ",", ".")}}</td>
                <td>${{number_format($payment -> descuento, 0, ",", ".")}}</td>
                <td>${{number_format($payment -> total, 0, ",", ".")}}</td>
                <td>
                  <a href="{{ action('FeesController@showFees',  array('id' => $payment->id)) }}">
                    <i class="fa fa-arrow-right"></i>
                  </a>
                </td>
              </tr>
              @endforeach
            @endforeach
          </tbody>
        </table>

      </div>
  </div>

</div>
@endsection
