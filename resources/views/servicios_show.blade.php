@extends('layouts.app')

@section('content')
<div class="container">
  <div id="breadcrumbs" class="breadcrumbs">
    <a href="/servicios">
      < SERVICIOS
    </a>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">{{$service -> name}}</div>

      <div class="panel-body">
          <p class='dialog'>
            {{$service -> description}}
          </p>
      </div>
  </div>

</div>
@endsection
