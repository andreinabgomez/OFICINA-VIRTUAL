@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Nuevo Servicio</div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="{{url('/servicios/new')}}">
               {{ csrf_field() }}
              <div class="col-md-5">
                  <input id="name" name="name" type="text" class="form-control" required autofocus placeholder="Nombre">
              </div>
              <div class="col-md-10 bottom">
                  <textarea id="description" name="description" required class="form-control" placeholder="Descripcion"></textarea>
              </div>
              <br>
              <div class="form-group">
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
