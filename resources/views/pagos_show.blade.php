@extends('layouts.app')

@section('content')
<div class="container">
  @if (Auth::user()->type == 'Empleado' || Auth::user()->type == 'Administrador' || Auth::user()->type == 'Cobranza')
    <div id="breadcrumbs" class="breadcrumbs">
      <a href="{{ action('ClientsController@showClient',  array('id' => $payment->client_service->client->id)) }}">
        < Cliente
      </a>
    </div>
  @endif
  <div class="panel panel-default">
      <div class="panel-heading">CUOTAS DEL PAGO - {{Carbon\Carbon::parse($payment->fecha_pago)->format('d-m-Y')}}</div>

      <div class="panel-body">
          <table class="table table-list">
            <thead>
              <tr>
                  <th>Fecha Vencimiento</th>
                  <th>Monto Cuotas</th>
                  <th>Intereses</th>
                  <th>Gasto Cobranza</th>
                  <th>Descuento</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($payment->fees as $fee)
              <tr>
                  <td>{{Carbon\Carbon::parse($fee->fecha_vencimiento)->format('d-m-Y')}}</td>
                  <td>${{number_format($fee->client_service->monto_pago, 0, ",", ".")}}</td>
                  <td>${{number_format($fee->interes, 0, ",", ".")}}</td>
                  <td>${{number_format($fee->gasto_cobranza, 0, ",", ".")}}</td>
                  <td>${{number_format($fee->descuento, 0, ",", ".")}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>

      </div>
  </div>

</div>
@endsection
