@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-9">
      <h4>CLIENTES VENCIDOS</h4>
    </div>
    <div class='col-md-3 div-boton'>
      {!! Form::open(['method'=>'GET','url'=>'clientes_vencidos','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

        <div class="input-group custom-search-form search_eliminar">
            <input type="search" name="search" placeholder="Buscar.." class='form-control'>
            <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Servicio</th>
          <th>Frecuencia</th>
          <th>Monto Cuota</th>
          <th>Fecha</th>
          <th>Dias vencidos</th>
          <th>Interes</th>
          <th>Gasto Cobranza</th>
          <th>Monto Total</th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $cliente)
        @if ($cliente->status == false)
          <tr>
              <td>{{$cliente->client->id_oficina}}</td>
              <td>{{$cliente->client->nombre}}</td>
              <td>{{$cliente->service->name}}</td>
              <td>{{$cliente->frecuencia}}</td>
              <td>${{number_format($cliente->monto_pago, 0, ",", ".")}}</td>
              <td>{{Carbon\Carbon::parse($cliente -> fecha_vencimiento)->format('d-m-Y')}}</td>
              <td>{{$cliente->dias_vencidos()}}</td>
              <td>${{number_format($cliente->intereses(), 0, ",", ".")}}</td>
              <td>${{number_format($cliente->gasto(), 0, ",", ".")}}</td>
              <td>${{number_format($cliente->total(), 0, ",", ".")}}</td>
              <td>
                <a href="{{ action('ClientsController@showClient',  array('id' => $cliente ->client->id)) }}">
                  <i class="fa fa-arrow-right"></i>
                </a>
              </td>
          </tr>
        @endif
      @endforeach
    </tbody>
  </table>
</div>
@endsection
