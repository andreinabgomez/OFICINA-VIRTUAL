@extends('layouts.app')

@section('content')
<div class="container">
  <div id="breadcrumbs" class="breadcrumbs">
    <a href="/clientes">
      < CLIENTES
    </a>
  </div>
  <div class="panel panel-default">
      <div class="panel-heading">INFORMACION</div>

      <div class="panel-body">
          <div class="col-md-12 titulo-col">
            <span class="precio titulo">PERSONAL:</span>
            <a href="{{ action('ClientsController@editClient',  array('id' => $client -> id)) }}">
              <i class="fa fa-pencil-square-o"></i>
            </a>
          </div>
          <div class="col-md-4">
            <p>
              <span class="precio">ID:</span>
              {{$client -> id_oficina}}
            </p>
            <p>
              <span class="precio">RUT:</span>
              {{$client -> rut}}-{{$client -> digito_verificador}}
            </p>
            <p>
              <span class="precio">Nombre:</span>
              {{$client -> nombre}}
            </p>
            <p>
              <span class="precio">Fecha Nacimiento:</span>
              {{Carbon\Carbon::parse($client->fecha_nacimiento)->format('d-m-Y')}}
            </p>
            <p>
              <span class="precio">Nacionalidad:</span>
              {{$client -> nacionalidad}}
            </p>
          </div>
          <div class="col-md-4 centro">
            <p>
              <span class="precio">Tlf Fijo:</span>
              {{$client -> telefono_fijo}}
            </p>
            <p>
              <span class="precio">Tlf Movil:</span>
              {{$client -> telefono_movil}}
            </p>
            <p>
              <span class="precio">Email:</span>
              {{$client -> email}}
            </p>
            <p>
              <span class="precio">Email Alternativo:</span>
              {{$client -> email_alternativo}}
            </p>
            <p>
              <span class="precio">Domicilio:</span>
              {{$client -> domicilio}}
            </p>
            <p>
              <span class="precio">Comuna:</span>
              {{$client -> comuna}}
            </p>
          </div>
          <div class="col-md-4">
            <p>
              <span class="precio">Empresa:</span>
              {{$client -> empresa}}
            </p>
            <p>
              <span class="precio">RUT Empresa:</span>
              {{$client -> rut_empresa}}
            </p>
            <p>
              <span class="precio">Actividad Empresa:</span>
              {{$client -> actividad_empresa}}
            </p>
            <p>
              @if ($client -> dicom==true)
                <span class="dicom">Registrado en DICOM</span>
              </p>
              <p>
                <br>
                <a class="btn-blue" href="{{ action('ClientsController@eliminarDicom',  array('id' => $client -> id)) }}">DESVINCULAR DE DICOM</a>
              @else
                <br>
                <a class="btn-blue" href="{{ action('ClientsController@registrarDicom',  array('id' => $client -> id)) }}">REGISTRAR EN DICOM</a>
              @endif
            </p>
          </div>
          <div class="col-md-12">
            <hr>
          </div>
          <div class="col-md-10 titulo-col">
            <span class="precio titulo">BITACORA DE LLAMADAS</span>
          </div>
          <div class ="col-md-2 div-boton">
            <a class="btn-border-blue" href="{{ action('ClientCallsController@newCall',  array('id' => $client -> id)) }}">NUEVA BITACORA</a>
          </div>
          <div class="col-md-6">
            @foreach ($client->calls->take(6) as $index => $call)
              <p>
                <span class="precio">{{Carbon\Carbon::parse($call->date)->format('d-m-Y g:i A')}}</span>
                {{$call -> resultado}}
              </p>
            @endforeach
          </div>
          <div class="col-md-12">
            <hr>
          </div>
          <div class="col-md-10 titulo-col">
            <span class="precio titulo">SERVICIOS</span>
          </div>
          <div class ="col-md-2 div-boton">
            <a class="btn-border-blue" href="{{ action('ClientServicesController@newService',  array('id' => $client -> id)) }}">NUEVO SERVICIO</a>
          </div>
          <table class="table table-list">
            <thead>
              <tr>
                  <th>Plan</th>
                  <th>Tipo Pago</th>
                  <th>Monto Cuota</th>
                  <th>Fecha Vencimiento</th>
                  <th>Cuotas Vencidas</th>
                  <th></th>
                  <th></th>
                  <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($client->clientservices->where('status', '=', false) as $client_service)
                <tr>
                    <td>{{$client_service -> service -> name}}</td>
                    <td>{{$client_service -> frecuencia}}</td>
                    <td>${{number_format($client_service -> monto_pago, 0, ",", ".")}}</td>
                    <td>{{Carbon\Carbon::parse($client_service -> fecha_vencimiento)->format('d-m-Y')}}</td>
                    <td>{{$client_service -> fees->where('payment', '=', false)->where('fecha_vencimiento', '<', Carbon\Carbon::now()) ->count()}}</td>
                    @if($client_service -> fecha_vencimiento > Carbon\Carbon::now())
                        <td>
                          <a href="{{ action('ClientServicesController@editService',  array('id' => $client_service -> id)) }}">
                            <i class="fa fa-pencil-square-o"></i>
                          </a>
                        </td>
                    @else
                      <td></td>
                    @endif
                    <td>
                      <a href="{{ action('ClientServicesController@deleteService',  array('id' => $client_service -> id)) }}">
                        <i class="fa fa-trash"></i>
                      </a>
                    </td>
                    <td><a href="{{ action('FeesController@newPayment',  array('id' => $client_service -> id)) }}">Pagar</a></td>
                </tr>
              @endforeach
            </tbody>
          </table>
          <div class="col-md-12">
            <hr>
          </div>
          <div class="col-md-10 titulo-col">
            <span class="precio titulo">PAGOS</span>
          </div>
          <table class="table table-list">
            <thead>
              <tr>
                  <th>Fecha Pago</th>
                  <th>Vencimiento</th>
                  <th>Servicio</th>
                  <th>Tipo Pago</th>
                  <th>Monto</th>
                  <th>Intereses</th>
                  <th>Gastos Cobranza</th>
                  <th>Descuento</th>
                  <th>Monto Cancelado</th>
                  <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($client->clientservices as $client_service)
                @foreach ($client_service->payments->take(3) as $payment)
                <tr>
                  <td>{{Carbon\Carbon::parse($payment -> fecha_pago)->format('d-m-Y')}}</td>
                  <td>{{Carbon\Carbon::parse($payment -> fecha_vencimiento)->format('d-m-Y')}}</td>
                  <td>{{$client_service -> service -> name}}</td>
                  <td>{{$payment -> forma_pago}}</td>
                  <td>${{number_format($payment -> monto_cuota, 0, ",", ".")}}</td>
                  <td>${{number_format($payment -> interes, 0, ",", ".")}}</td>
                  <td>${{number_format($payment -> gasto_cobranza, 0, ",", ".")}}</td>
                  <td>${{number_format($payment -> descuento, 0, ",", ".")}}</td>
                  <td>${{number_format($payment -> total, 0, ",", ".")}}</td>
                  <td>
                    <a href="{{ action('FeesController@showFees',  array('id' => $payment->id)) }}">
                      <i class="fa fa-arrow-right"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              @endforeach
            </tbody>
          </table>
      </div>
  </div>

</div>
@endsection
