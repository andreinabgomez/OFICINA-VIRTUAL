@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-4">
      <h4>CLIENTES</h4>
    </div>
    <div class='col-md-3 search-div'>
      {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'search'])  !!}

        <div class="input-group custom-search-form">
            <input type="search" name="search" placeholder="Buscar.." class='form-control'>
            <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
      {!! Form::close() !!}

    </div>
    <div class='col-md-3 search-div'>
      {!! Form::open(['method'=>'GET','url'=>'clientes','class'=>'navbar-form navbar-left','role'=>'id'])  !!}

        <div class="input-group custom-search-form">
            <input type="search" name="id" placeholder="Buscar por ID.." class='form-control'>
            <span class="input-group-btn">
                <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
      {!! Form::close() !!}

    </div>
    <div class ="col-md-2 div-boton">
      <a class="btn-border-blue" href="/public/clientes/new">NUEVO CLIENTE</a>
    </div>
  </div>
  <table class="table table-list sortable">
    <thead>
      <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Servicio</th>
          <th>Frecuencia</th>
          <th>Monto</th>
          <th>Estatus</th>
          <th></th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $cliente)
        @if ($cliente->status == false)
          <tr>
              <td>{{$cliente->client->id_oficina}}</td>
              <td>{{$cliente->client->nombre}}</td>
              <td>{{$cliente->service->name}}</td>
              <td>{{$cliente->frecuencia}}</td>
              <td>${{number_format($cliente->total(), 0, ",", ".")}}</td>
              <td>
                <span class="status {{$cliente->status_css()}}">{{$cliente->status()}}</span>
              </td>
              <td>
                <a href="{{ action('ClientsController@showClient',  array('id' => $cliente ->client->id)) }}">
                  <i class="fa fa-arrow-right"></i>
                </a>
              </td>
          </tr>
        @endif
      @endforeach
    </tbody>
  </table>
  @include('pagination.default', ['paginator' => $clients])
</div>
@endsection
