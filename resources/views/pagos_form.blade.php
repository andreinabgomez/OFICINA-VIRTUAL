@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Registro Pago - {{$client -> client -> id_oficina}} - {{$client -> client -> nombre}} </div>
      <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST">
            {{ csrf_field() }}
              <div class="form-group">
                <table class="table table-list">
                  <thead>
                    <tr>
                        <th></th>
                        <th>Fecha de Vencimiento</th>
                        <th>Mensualidad</th>
                        <th>Dias de atraso</th>
                        <th>Intereses</th>
                        <th>Gasto Cobranza</th>
                        <th>Monto Total</th>
                        <th>Descuento</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($client->fees->where('payment', '=', false) as $fee)
                      <tr>
                          <td>{{  Form::checkbox($fee -> id, 'value')}}</td>
                          <td>{{Carbon\Carbon::parse($fee -> fecha_vencimiento)->format('d-m-Y')}}</td>
                          <td>${{number_format($client -> monto_pago, 0, ",", ".")}}</td>
                          <td>{{$fee -> dias_vencidos()}}</td>
                          <td> <input name = 'interes_{{$fee -> id}}' type="text" class="form-control" autofocus value="{{$fee -> interes}}"></td>
                          <td> <input name = 'gasto_{{$fee -> id}}' type="text" class="form-control" autofocus value="{{$fee -> gasto_cobranza}}"> </td>
                          <td> {{number_format($fee -> total, 0, ",", ".")}}</td>
                          <td> <input name = 'descuento_{{$fee -> id}}' type="text" class="form-control" autofocus value="{{$fee -> descuento}}"></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="col-md-2 col-md-offset-10">
                    <button type="submit" class="btn btn-blue div-cancelar-pago">
                        Calcular Pago
                    </button>
                </div>
              </form>
                <div class="col-md-12">
                  <hr>
                </div>
              <form method="post" action="{{ action('FeesController@makePayment', array('id' => $client -> id)) }}" accept-charset="UTF-8">
                {{ csrf_field() }}
                @foreach ($ids as $id)
                  {{ Form::hidden($id, $id) }}
                @endforeach
                  {{ Form::hidden("intereses", $monto_interes) }}
                  {{ Form::hidden("gastos", $monto_gasto) }}
                  {{ Form::hidden("descuentos", $monto_descuento) }}
                  {{ Form::hidden("total", $monto_total) }}
                  {{ Form::hidden("cuotas", $monto_cuotas) }}
                <div class="col-md-4">
                    <table class="table table-list">
                      <tbody>
                        <tr>
                          <td>Total Monto Cuota</td>
                          <td>${{number_format($monto_cuotas, 0, ",", ".")}}</td>
                        </tr>
                        <tr>
                            <td>Total Intereses</td>
                            <td>${{number_format($monto_interes, 0, ",", ".")}}</td>
                        </tr>
                        <tr>
                            <td>Total Gasto Cobranza</td>
                            <td>${{number_format($monto_gasto, 0, ",", ".")}}</td>
                        </tr>
                        <tr>
                            <td>Total Descuento</td>
                            <td>${{number_format($monto_descuento, 0, ",", ".")}}</td>
                        </tr>
                        <tr>
                            <td>Total Monto A Pagar</td>
                            <td>${{number_format($monto_total, 0, ",", ".")}}</td>
                        </tr>
                      </tbody>
                    </table>
                </div>
                @if (Auth::user()->type == 'Empleado' || Auth::user()->type == 'Administrador')
                    <div class="col-md-4">
                      <select name='forma_pago' class="form-control">
                        <option value="" selected>Selecciona el tipo de Pago</option>
                        <option value="Efectivo">Efectivo</option>
                        <option value="Transferencia">Transferencia</option>
                        <option value="Tarjeta">Tarjeta</option>
                        <option value="Deposito">Deposito</option>
                      </select>
                    </div>
                    <div class="col-md-4">
                      <div id = 'calendar' class="input-group date">
                        <input type="text" name='date' placeholder="Fecha Pago" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                      </div>
                    </div>
                  <div class="form-group">
                      <div class="col-md-1 col-md-offset-10">
                          <button type="submit " class="btn btn-blue div-guardar">
                              Guardar
                          </button>
                      </div>
                      <div class="col-md-1">
                          <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                      </div>
                  </div>
                @endif

            </form>
      </div>
  </div>

</div>
</div>
@endsection
