@extends('layouts.app')

@section('content')
<div class="container">
  <div class = "row">
    <div class ="col-md-10">
      <h4>PAGOS UNICOS</h4>
    </div>
    <div class ="col-md-2 div-boton">
      <a class="btn-border-blue" href="/pagos_unicos/new">NUEVO PAGO</a>
    </div>
  </div>
        <table class="table table-list sortable">
          <thead>
            <tr>
                <th>Fecha Pago</th>
                <th>Nombre</th>
                <th>RUT</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Servicio</th>
                <th>Monto</th>
                <th>Forma Pago</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($clients as  $payment)
              <tr>
                <td>{{Carbon\Carbon::parse($payment -> date)->format('d-m-Y')}}</td>
                <td>{{$payment -> nombre}}</td>
                <td>{{$payment -> rut}}-{{$payment -> digito_verificador}}</td>
                <td>{{$payment -> email}}</td>
                <td>{{$payment -> telefono}}</td>
                <td>{{$payment -> service -> name}}</td>
                <td>${{number_format($payment -> monto_pago, 0, ",", ".")}}</td>
                <td>{{$payment -> forma_pago}}</td>
            @endforeach
          </tbody>
        </table>
        @include('pagination.default', ['paginator' => $clients])
</div>
@endsection
