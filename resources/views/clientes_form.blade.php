@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Nuevo Cliente</div>
      <div class="panel-body">
        <form class="form-horizontal" role="form" method="POST" action="{{url('/clientes/new')}}">
             {{ csrf_field() }}
              <div class="col-md-12 titulo-col">
                <span class="precio titulo">INFORMACION PERSONAL</span>
              </div>
              <div class="col-md-2">
                <input id="id" name='id' type="text" class="form-control" required autofocus placeholder="ID">
              </div>
              <div class="col-md-1">
              </div>
              <div class="col-md-2">
                <input type="text" name='rut' class="form-control" required autofocus placeholder="RUT">
              </div>
              <div class="col-md-1">
                <input  type="text" name='digito' class="form-control" required autofocus>
              </div>
              <div class="col-md-3">
                  <input type="text" name='name' class="form-control" required autofocus placeholder="Nombre">
              </div>
              <div class="col-md-3">
                <div id = 'calendar' class="input-group date">
                  <input type="text" required  name='date' placeholder="Fecha Nacimiento" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                </div>
              </div>
              <div class="col-md-3 bottom">
                  <input name='nacionalidad' type="text" class="form-control" autofocus placeholder="Nacionalidad">
              </div>
              <div class="col-md-3 bottom">
                  <input  name='fijo' type="text" class="form-control" autofocus placeholder="Tlf Fijo">
              </div>
              <div class="col-md-3 bottom">
                  <input name='movil' type="text" class="form-control" required autofocus placeholder="Tlf Movil">
              </div>
              <div class="col-md-3 bottom">
                  <input name='email' type="text" class="form-control" required autofocus placeholder="Email">
              </div>
              <div class="col-md-3 bottom">
                  <input name='alternativo' type="text" class="form-control" autofocus placeholder="Email Alternativo">
              </div>
              <div class="col-md-6 bottom">
                  <input name='domicilio' type="text" class="form-control" autofocus placeholder="Domicilio">
              </div>
              <div class="col-md-3 bottom">
                  <input name='comuna' type="text" class="form-control" autofocus placeholder="Comuna">
              </div>
              <div class="col-md-12">
                <hr>
                <span class="precio titulo">INFORMACION DE EMPRESA</span>
              </div>
              <div class="col-md-3 bottom">
                  <input name='empresa' type="text" class="form-control" autofocus placeholder="Empresa">
              </div>
              <div class="col-md-3 bottom">
                  <input name='rut-empresa' type="text" class="form-control" autofocus placeholder="RUT">
              </div>
              <div class="col-md-6 bottom">
                  <input name='actividad_empresa' type="text" class="form-control" autofocus placeholder="Actividad">
              </div>
              <div class="col-md-12">
                <hr>
                <span class="precio titulo">SERVICIO</span>
              </div>
              <div class="col-md-3 bottom">
                <select name='servicio' required  class="form-control">
                  <option value="" selected>Selecciona el Plan</option>
                  @foreach($services as $service)
                    <option value="{{$service->id}}">{{$service->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-3 bottom">
                <select name='dia' required class="form-control">
                  <option value="" selected>Selecciona el dia de pago</option>
                  <option value="05">05</option>
                  <option value="10">10</option>
                  <option value="15">15</option>
                  <option value="20">20</option>
                  <option value="25">25</option>
                  <option value="30">30</option>
                </select>
              </div>
              <div class="col-md-3 bottom">
                <select name='frecuencia' required class="form-control">
                  <option value="" selected>Selecciona el tipo de pago</option>
                  <option value="Semestral">Semestral</option>
                  <option value="Anual">Anual</option>
                  <option value="Trimestral">Trimestral</option>
                  <option value="Mensual">Mensual</option>
                  <option value="Unica">Una vez</option>
                </select>
              </div>
              <div class="col-md-3 bottom">
                  <input name='mensualidad' required type="text" class="form-control" autofocus placeholder="Mensualidad">
              </div>
              <div class="col-md-3 bottom">
                  <input name='inscripcion'  type="text" class="form-control" autofocus placeholder="Inscripcion">
              </div>
              <div class="col-md-3 bottom">
                <div id = 'calendar_1' required class="input-group date">
                  <input type="text" name='creacion_servicio' placeholder="Fecha primer pago" class="form-control"><span class="input-group-addon"><i class="fa fa-calendar icon-calendar"></i></span>
                </div>
              </div>
              <div class="col-md-3 bottom">
                <select name='forma_pago' required class="form-control">
                  <option value="" selected>Selecciona la forma del primer pago</option>
                  <option value="Efectivo">Efectivo</option>
                  <option value="Transferencia">Transferencia</option>
                  <option value="Tarjeta">Tarjeta</option>
                  <option value="Deposito">Deposito</option>
                </select>
              </div>

              <div class="form-group">
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>
</div>
@endsection
