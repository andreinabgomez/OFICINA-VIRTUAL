@extends('layouts.app')

@section('content')
<div class="container">
  <div class="panel panel-default">
      <div class="panel-heading">Registro Bitacora - {{$client -> id_oficina}} - {{$client -> nombre}}</div>
      <div class="panel-body">
          <p>
            <span class="precio">Tlf Fijo:</span>
            {{$client -> telefono_fijo}}
          </p>
          <p>
            <span class="precio">Tlf Movil:</span>
            {{$client -> telefono_movil}}
          </p>
          <form class="form-horizontal" role="form" method="POST">
              {{ csrf_field() }}
              <div class="col-md-7">
                <textarea required id='resultado' name="resultado" class="form-control" placeholder="Resultado"></textarea>
              </div>
              <div class="form-group">
                  <div class="col-md-1 col-md-offset-10">
                      <button type="submit " class="btn btn-blue div-guardar">
                          Guardar
                      </button>
                  </div>
                  <div class="col-md-1">
                      <a class="btn-border-blue div-cancelar" href="{{ URL::previous() }}">Cancelar</a>
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
